// Get the current date
var currentTime = new Date()
var month = currentTime.getMonth() + 1
var month = month.toString()
var month = month.padStart(2, "0")
var year = currentTime.getFullYear()
var year = year.toString()

// Add togther api request elements
var apiRequest = "https://api.gbif.org/v1/occurrence/search?countryCode=NZ&stateProvince=Canterbury&limit=300&offset=0&eventDate=" + year + "-" + month

// Fetch observations
console.log(apiRequest)
var apitest = apiRequest

// Display observations on map
function makecircle(responseText)
{
	var response = JSON.parse(responseText)
	console.log(response)
	console.log([response.results[0].decimalLatitude, response.results[0].decimalLongitude])
	for (var i = 0; i < response.results.length; i++) {
		placeDot([response.results[i].decimalLatitude, response.results[i].decimalLongitude],response.results[i].species + "\n" + response.results[i].occurrenceID, response.results[i].coordinateUncertaintyInMeters)
	}

}

function httpGetAsync(theUrl, callback)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous
    xmlHttp.send(null);
}

httpGetAsync(apitest, makecircle);

	var mymap = L.map('mapid').setView([51.505, -0.09], 13);

//This loads the streets for the map from open streetmaps

	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox.streets'
	}).addTo(mymap);

//This creates a marker on the map

L.marker([-43.531432, 172.626951]).addTo(mymap)
		.bindPopup("<b>A fountain</b><br />It's fairly iconic").openPopup();

function placeDot(loc, name, error) {
	if (error < 500) {
	L.circle(loc, error, {
		color: 'red',
		fillColor: '#f03',
		fillOpacity: 0.05
	}).addTo(mymap).bindPopup(name);
}
}

/*
L.polygon([
			[-44, 172.2],
			[-44, 173.3],
			[-43.3, 173.3],
			[-43.3, 172.2]
		]).addTo(mymap).bindPopup("This will be our default area of interest.");
*/

	var popup = L.popup();

	function onMapClick(e) {
		popup
			.setLatLng(e.latlng)
			.setContent("The coordinates for this spot are " + e.latlng.toString())
			.openOn(mymap);
	}

	mymap.on('click', onMapClick);
